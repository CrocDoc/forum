from django.apps import AppConfig


class ForumWebConfig(AppConfig):
    name = 'forum_web'
