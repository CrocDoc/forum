from django.views import generic
from django.contrib.auth.views import LoginView, LogoutView
from .models import Post, Comment
from django.urls import reverse_lazy
from django.contrib.auth import logout, models
from .forms import CommentForm, PostForm, LoginForm, RegistrationForm
from django.http import HttpResponseRedirect


class IndexView(generic.RedirectView):
    url = reverse_lazy('forum:post_list')


# Auth views
class Login(LoginView):
    redirect_field_name = ''
    redirect_authenticated_user = True
    authentication_form = LoginForm
    template_name = 'registration/login.html'


class Logout(LogoutView):
    url = reverse_lazy('forum:post_list')

    def get_redirect_url(self, *args, **kwargs):
        logout(self.request)
        return super().get_redirect_url()


class UserDetail(generic.DetailView):
    model = models.User
    template_name = 'forum_web/user_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['posts'] = Post.objects.filter(user=self.request.user.id)
        return context


class Registration(generic.CreateView):
    form_class = RegistrationForm
    template_name = 'registration/registration.html'
    success_url = reverse_lazy('forum:post_list')


# Post views
class PostList(generic.ListView):
    model = Post
    template_name = 'forum_web/post_list.html'
    queryset = Post.objects.all()
    context_object_name = 'posts'
    paginate_by = 20


class PostDetail(generic.DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super(PostDetail, self).get_context_data(**kwargs)
        context['comment_form'] = CommentForm
        context['comments'] = Comment.objects.filter(post=context['post'].id).order_by('-created_at')
        return context


class PostCreate(generic.CreateView):
    model = Post
    form_class = PostForm
    template_name = 'forum_web/post_edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['view_type'] = 'Create'
        return context

    def form_valid(self, form):
        post = form.save(commit=False)
        post.user = models.User.objects.get(id=self.request.user.id)
        post.save()
        return HttpResponseRedirect(reverse_lazy('forum:user_detail', kwargs={'pk': self.request.user.id}))

    def get_success_url(self):
        return reverse_lazy('forum:user_detail', kwargs={'pk': self.request.user.id})


class PostUpdate(generic.UpdateView):
    model = Post
    form_class = PostForm
    template_name = 'forum_web/post_edit.html'

    def get_success_url(self):
        return reverse_lazy('forum:user_detail', kwargs={'pk': self.request.user.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['view_type'] = 'Update'
        return context


class PostDelete(generic.DeleteView):
    model = Post
    template_name = 'forum_web/post_delete.html'

    def get_success_url(self):
        return reverse_lazy('forum:user_detail', kwargs={'pk': self.request.user.id})


# Comment views
class CommentCreate(generic.CreateView):
    model = Comment
    form_class = CommentForm

    def form_valid(self, form):
        comment = form.save(commit=False)
        comment.post = Post.objects.get(id=self.request.POST['post'])
        comment.user = models.User.objects.get(id=self.request.user.id)
        comment.save()
        return HttpResponseRedirect(reverse_lazy('forum:post_detail', kwargs={'pk': self.request.POST['post']}))


class CommentDelete(generic.DeleteView):
    model = Comment
    template_name = 'forum_web/comment_delete.html'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = reverse_lazy('forum:post_detail', kwargs={'pk': self.object.post.id})
        self.object.delete()
        return HttpResponseRedirect(success_url)
