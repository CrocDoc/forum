from django.forms import ModelForm, fields
from .models import Post, Comment
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm


class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username', 'password']


class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username']


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['category', 'title', 'text']


class CommentForm(ModelForm):
    post = fields.HiddenInput()

    class Meta:
        model = Comment
        fields = ['text']
