from django.urls import path
from . import views
app_name = 'forum'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    # Auth urls
    path('login/', views.Login.as_view(), name='login'),
    path('logout/', views.Logout.as_view(), name='logout'),
    path('register/', views.Registration.as_view(), name='registration'),
    # Post urls
    path('posts/', views.PostList.as_view(), name='post_list'),
    path('posts/<int:pk>', views.PostDetail.as_view(), name='post_detail'),
    path('posts/create', views.PostCreate.as_view(), name='post_create'),
    path('posts/update/<int:pk>', views.PostUpdate.as_view(), name='post_update'),
    path('posts/delete/<int:pk>', views.PostDelete.as_view(), name='post_delete'),
    # Comment urls
    path('comments/create', views.CommentCreate.as_view(), name='comment_create'),
    path('comment/delete/<int:pk>', views.CommentDelete.as_view(), name='comment_delete'),
    # User urls
    path('user/<int:pk>', views.UserDetail.as_view(), name='user_detail')
]
